/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombme;

import bombme.Values.NuclearCellStatus;


/**
 *
 * @author siva1
 */
public class NuclearCell {
    private int row;
    private int column;
    private NuclearCellStatus status;
    public NuclearCell(int r,int c){
        this.row=r;
        this.column=c;
        this.status=NuclearCellStatus.EMPTY;
    }
    public int getRow(){
        return this.row;
    }
    public int getColumn(){
        return this.column;
    }
    public NuclearCellStatus getStatus(){
        return this.status;
    }
    public void setStatus(NuclearCellStatus status, long key ) {
        
        if(Values.Diffusal_Key == key){
            this.status=status;
        }
    }
    
}
