/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package bombme;
import bombme.Values.NuclearCellStatus;
import bombme.Values.PlayerType;
import java.util.Arrays;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author siva1
 */
public class GameController {
    private Bott user,comp;
    private NuclearReactor cb;
    private PlayerType currentplayer;
    static public JSONObject jb;
    public int toss(){
        int num=(int) Math.random();
        return num%2;
    }
    
    public void play(){
        while(cb.getRecentOpponentMove().getStatus()==NuclearCellStatus.EMPTY || cb.isGameover() == PlayerType.NOPLAYER){
            if(currentplayer==PlayerType.USER){
                cb.dropBomb(user.calculateNextMove(cb), currentplayer);
                currentplayer=PlayerType.COMP;
            }else{
                cb.dropBomb(comp.calculateNextMove(cb), currentplayer);
                currentplayer=PlayerType.USER;
            }
        }
        String winner;
        if(cb.getRecentOpponentMove().getStatus()==NuclearCellStatus.BOMB){
            winner=Values.userbot;
        }else{
            winner=Values.compbot;
        }
       cb.JSONwinner(winner);
    }
    public GameController(){
        if(toss()==0){
            cb=new NuclearReactor(PlayerType.USER);
            currentplayer=PlayerType.USER;
        }else{
            cb=new NuclearReactor(PlayerType.COMP);
            currentplayer=PlayerType.COMP;
        }
        user=new UserBot(PlayerType.USER);
        comp=new UserBot(PlayerType.COMP);
        play();
    }
}
