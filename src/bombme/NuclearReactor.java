/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package bombme;

import static bombme.GameController.jb;
import bombme.Values.NuclearCellStatus;
import bombme.Values.PlayerType;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



/**
 *
 * @author siva1
 */
public class NuclearReactor {
    NuclearCell cells[][];
    NuclearCell lastmove;
    public JSONObject jb;
    JSONArray ja;
    public NuclearReactor(PlayerType player){
        
        cells=new NuclearCell[Values.row_num][Values.col_num];
        
        initialiseBoard();
        this.lastmove=new NuclearCell(-1,-1);
        this.lastmove.setStatus(NuclearCellStatus.EMPTY,Values.Diffusal_Key);
        ja=new JSONArray();
        jb=new JSONObject();
    }
    public NuclearCell getRecentOpponentMove(){
        return this.lastmove;
    }
    public NuclearCellStatus getCellStatus(int r,int c){
        try{
        return cells[r][c].getStatus();
        }catch(ArrayIndexOutOfBoundsException ae){
            return NuclearCellStatus.INVALID;
        }
    }
    public boolean isBombPlaceable(int col){
        //System.out.println(col);
        if(cells[0][col].getStatus()==NuclearCellStatus.EMPTY){
            return true;
        }else{
            return false;
        }
    }
    public void JSONwinner(String player){
        jb.put("winner",player);
    }
    public void JSONformatter(String winner,JSONArray winmoves){
        jb.put("winner",winner);
        jb.put("moves", ja);
        jb.put("coordinates",winmoves);
        String wd=System.getProperty("user.dir");
        
        FileWriter fw;
        try {
            fw = new FileWriter(wd+"/moves.json");
            fw.write(jb.toJSONString());
            fw.flush();
        } catch (IOException ex) {
            Logger.getLogger(NuclearReactor.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(jb);
       
    }
    public void dropBomb(int col,PlayerType player){
        JSONObject j=new JSONObject();
        String botname;
        NuclearCellStatus status=null;
        if(player==PlayerType.USER){
            status=NuclearCellStatus.BOMB;
            botname=Values.userbot;
        }else{
            botname=Values.compbot;
            status=NuclearCellStatus.DEFUSER;
        }
        if(isBombPlaceable(col)){
            int row=(Values.row_num)-1;
            //System.out.println(row);
            while(cells[row][col].getStatus()!=NuclearCellStatus.EMPTY){
                row--;
            }
          
            //();
            cells[row][col].setStatus(status,Values.Diffusal_Key);
            lastmove=new NuclearCell(row,col);
            lastmove.setStatus(status,Values.Diffusal_Key);
            j.put("player",botname);
            j.put("row",row);
            j.put("column",col);
            ja.add(j);
            //System.out.println(player+" "+col+" "+color);
            
        }else{
            if(player==PlayerType.USER){
                JSONwinner(Values.compbot);
            }else{
                JSONwinner(Values.userbot);
            }
        }
    }
    public PlayerType isGameover(){
        int k;
        String winner;
        int directions[]=new int[8];
        boolean flags[]=new boolean[8];
        Arrays.fill(directions,0);
        //(directions);
        Arrays.fill(flags,Boolean.FALSE);
        NuclearCell bc=lastmove;
        int row = bc.getRow();
        int column=bc.getColumn();
        NuclearCellStatus status=bc.getStatus();
        if(status==NuclearCellStatus.BOMB){
            winner=Values.userbot;
        }else{
            winner=Values.compbot;
        }
        JSONArray winmoves=new JSONArray();
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 8; j++){
                if(directions[j] == i && !flags[j]){
                    switch(j){
                        case 0:
                            if(row-i-1 >= 0 && row-i-1 < Values.row_num && column-i-1 >=0 && column-i-1 < Values.col_num){
                                if(getCellStatus(row-i-1,column-i-1) == status){
                                    directions[j]++;
                                    
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 1:
                            if(row-i-1 >= 0 && row-i-1 < Values.row_num){
                                if(getCellStatus(row-i-1,column) == status){
                                    directions[j]++;
                                    
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 2:
                            if(row-i-1 >= 0 && row-i-1 < Values.row_num && column+i+1 >=0 && column+i+1 < Values.col_num){
                                if(getCellStatus(row-i-1,column+i+1) == status){
                                    directions[j]++;
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 3:
                            if(column+i+1 >=0 && column+i+1 < Values.col_num){
                                if(getCellStatus(row,column+i+1) == status){
                                    directions[j]++;
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 4:
                            if(row+i+1 >= 0 && row+i+1 < Values.row_num && column+i+1 >=0 && column+i+1 < Values.col_num){
                                if(getCellStatus(row+i+1,column+i+1) == status){
                                    directions[j]++;
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 5:
                            if(row+i+1 >= 0 && row+i+1 < Values.row_num){
                                if(getCellStatus(row+i+1,column) == status){
                                    directions[j]++;
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 6:
                            if(row+i+1 >= 0 && row+i+1 < Values.row_num && column-i-1 >=0 && column-i-1 < Values.col_num){
                                if(getCellStatus(row+i+1,column-i-1) == status){
                                    directions[j]++;
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                        case 7:
                            if(column-i-1 >=0 && column-i-1 < Values.col_num){
                                if(getCellStatus(row,column-i-1) == status){
                                    directions[j]++;
                                }
                                else{
                                    flags[j] = !flags[j];
                                }
                            }
                            break;
                    }
                    
                    
                }
            }
        }
        int points[][]=new int[3][2];
        JSONObject lastmoves[]=new JSONObject[4];
        for(int m=0;m<4;m++){
            lastmoves[m]=new JSONObject();
        }
        if((directions[0]+directions[4]) >= 3||(directions[1]+directions[5]) >= 3||(directions[2]+directions[6]) >= 3||(directions[3]+directions[7]) >= 3){
            if((directions[0]+directions[4]) >= 3){
                int i = 0;
                for(int j = 0; i<3 && j < directions[0]; i++,j++){
                    points[i][0] = row - 1 - j;
                    
                    points[i][1] = column - 1 - j;
                }
                for(int j = 0; i<3 && j < directions[4]; i++,j++){
                    points[i][0] = row + 1 + j;
                    points[i][1] = column + 1 + j;
                }
                
                
            }else if((directions[1]+directions[5]) >= 3){
                int i = 0;
                for(int j = 0; i<3 && j < directions[1]; i++,j++){
                    points[i][0] = row - 1 - j;
                    points[i][1] = column;
                }
                for(int j = 0; i<3 && j < directions[5]; i++,j++){
                    points[i][0] = row + 1 + j;
                    points[i][1] = column;
                }
                
                
            }else if((directions[2]+directions[6]) >= 3){
                int i = 0;
                for(int j = 0; i<3 && j < directions[2]; i++,j++){
                    points[i][0] = row - 1 - j;
                    points[i][1] = column + 1 + j;
                }
                for(int j = 0; i<3 && j < directions[6]; i++,j++){
                    points[i][0] = row + 1 + j;
                    points[i][1] = column - 1 - j;
                }
                
                
            }else if((directions[3]+directions[7]) >= 3){
                int i = 0;
                for(int j = 0; i<3 && j < directions[3]; i++,j++){
                    points[i][0] = row;
                    points[i][1] = column + 1 + j;
                }
                for(int j = 0; i<3 && j < directions[7]; i++,j++){
                    points[i][0] = row;
                    points[i][1] = column - 1 - j;
                }
                
                
            }
            for(int j = 0; j < 3; j++ ){
                //System.out.println(points[j][0] + "," + points[j][1]);
                lastmoves[j].put("row", points[j][0]);
                lastmoves[j].put("column", points[j][1]);
                winmoves.add(lastmoves[j]);
            }
            lastmoves[3].put("row",row);
            lastmoves[3].put("column",column);
            winmoves.add(lastmoves[3]);
            //System.out.println(lastmoves[3]);
            JSONformatter(winner,winmoves);
            
            if(getRecentOpponentMove().getStatus() == NuclearCellStatus.BOMB){
                return PlayerType.USER;
            }
            else{
                return PlayerType.COMP;
            }
        }
        for(k = 0; k < Values.col_num; k++){
            if(isBombPlaceable(k)){
                break;
            }
        }
        if(k==Values.col_num){
            JSONformatter("draw",winmoves);
        }
        if(row==-1 && column==-1){
            return PlayerType.NOPLAYER;
        }
        return PlayerType.NOPLAYER;
    }
    
    private void initialiseBoard() {
        
        for(int i=0;i<Values.row_num;i++){
            for(int j=0;j<Values.col_num;j++){
                cells[i][j]=new NuclearCell(i,j);
            }
        }
    }
}
