/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombme;

import bombme.Values.PlayerType;

/**
 *
 * @author siva1
 */
public class UserBot extends Bott{

    public UserBot(PlayerType player) {
        super(player);
    }

    @Override
    int calculateNextMove(NuclearReactor cb) {
        int x = (int) (Math.random()*(Values.col_num));
        
        while(!cb.isBombPlaceable(x)) {
            x = (int) (Math.random()*(Values.col_num));
        }
        return x;
    }
    
}
